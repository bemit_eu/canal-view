# Flood\Canal: View

View files for [Flood\Canal](https://canal.bemit.codes) and [Flood\Hydro](https://hydro.bemit.codes) projects or use as twig template base, see [documentation of canal-view](https://bemit.codes/canal-view). 

# Update to new File Structure and Blockmodel in 0.9.0

See `rename.php`, `wireframe.png` and `src/base.twig`, mostly this is sufficient from the root view folder:

    php ../vendor/flood/canal-view/rename.php

# Licence

This project is free software distributed under the terms of two licences, the CeCILL-C and the GNU Lesser General Public License. You can use, modify and/ or redistribute the software under the terms of CeCILL-C (v1) for Europe or GNU LGPL (v3) for the rest of the world.

This file and the LICENCE.* files need to be distributed and not changed when distributing.
For more informations on the licences which are applied read: [LICENCE.md](LICENCE.md)

# Copyright

2017-2019 | [bemit UG (haftungsbeschränkt)](https://bemit.eu) - project@bemit.codes

Maintainer: [Michael Becker](https://mlbr.xyz)