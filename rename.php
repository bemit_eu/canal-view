<?php

// automatically mv view files and rename blocks to new canal-view naming and file structure
// execute from console in the root view directory:
// php rename.php

$mv = [
    'block'                        => 'layout',
    'fragment/embed'               => 'component/embed',
    'fragment/form'                => 'component/form',
    'general/base.twig'            => 'base.twig',
    'layout/content.twig'          => 'layout/content-main.twig',
    'layout/footer.twig'           => 'layout/footer-main.twig',
    'layout/footer-copyright.twig' => 'layout/footer-end.twig',
    'layout/sidebar-left.twig'     => 'layout/sidebar-left-main.twig',
    'layout/sidebar-right.twig'    => 'layout/sidebar-right-main.twig',
];

// Rename the blocks which can be identified by their position and meaning, not all existing blocks could be renamed automatically, see the new `base.twig` and the `wireframe.png` for more infos on the new model

$rename = [
    '{% block container_outer %}'     => '{% block container_outer %}',
    '{% block container_inner_raw %}' => '{% block container_inner_raw %}',

    '{% block header_raw %}'   => '{% block header_raw %}',
    '{% block header_inner %}' => '{% block header_inner %}',

    '{% block sidebar_left_raw %}'       => '{% block sidebar_left_raw %}',
    '{% block sidebar_left_inner_raw %}' => '{% block sidebar_left_inner_raw %}',
    '{% block sidebar_left_main %}'      => '{% block sidebar_left_main %}',

    '{% block sidebar_right_raw %}'       => '{% block sidebar_right_raw %}',
    '{% block sidebar_right_inner_raw %}' => '{% block sidebar_right_inner_raw %}',
    '{% block sidebar_right_main %}'      => '{% block sidebar_right_main %}',

    '{% block content_raw %}'         => '{% block content_raw %}',
    '{% block content_raw %}'             => '{% block content_raw %}',
    '{% block content_begin_inner %}' => '{% block content_begin_inner %}',
    '{% block content_main_inner %}'  => '{% block content_main_inner %}',
    '{% block content_end_inner %}'   => '{% block content_end_inner %}',

    '{% block footer_raw %}'             => '{% block footer_raw %}',
    '{% block footer_main_inner %}'      => '{% block footer_main_inner %}',
    '{% block footer_copyright_inner %}' => '{% block footer_copyright_inner %}',
];

// moving
foreach($mv as $src => $dist) {
    if(file_exists(__DIR__ . '/' . $src)) {
        rename(__DIR__ . '/' . $src, __DIR__ . '/' . $dist);
    }
}

// renaming

// building separate arrays
$rename_src = [];
$rename_dist = [];
foreach($rename as $src => $dist) {
    $rename_src[] = $src;
    $rename_dist[] = $dist;
}

// reading files and replacing contents
$rii = new RecursiveIteratorIterator(new RecursiveDirectoryIterator(__DIR__, RecursiveDirectoryIterator::SKIP_DOTS));
foreach($rii as $file) {
    if($file->isDir()) {
        continue;
    }

    $content = file_get_contents($file->getPathname());
    $content = str_replace($rename_src, $rename_dist, $content);
    file_put_contents($file->getPathname(), $content);
}
